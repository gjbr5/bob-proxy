#include <arpa/inet.h>
#include <iostream>
#include <mutex>
#include <netdb.h>
#include <semaphore.h>
#include <string.h>
#include <sys/socket.h>
#include <thread>
#include <unistd.h>

#define reint_cast reinterpret_cast

[[noreturn]] void die(const char *msg, int code = 1)
{
    std::cerr << msg;
    exit(code);
}

void relay(int from, int to)
{
    ssize_t relaylen;
    char relay_buf[BUFSIZ] = {0};
    while ((relaylen = recv(from, relay_buf, BUFSIZ - 1, 0)) > 0)
        send(to, relay_buf, static_cast<size_t>(relaylen), 0);

    close(to);
}

int main()
{
    int proxy_sock = socket(PF_INET, SOCK_STREAM, 0);
    struct sockaddr_in proxy_addr = {AF_INET, htons(8080), {htonl(INADDR_ANY)}, {0}};
    int option = 1; // SO_REUSEADDR 의 옵션 값을 TRUE 로
    setsockopt(proxy_sock, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

    if (proxy_sock < 0)
        die("Failed to create proxy socket\n");
    if (bind(proxy_sock, reint_cast<struct sockaddr *>(&proxy_addr), sizeof(proxy_addr)) < 0)
        die("Failed to binding proxy socket\n");
    if (listen(proxy_sock, 100) < 0)
        die("Failed to listening proxy socket\n");

    while (true) {
        struct sockaddr_in client_addr;
        socklen_t client_len = sizeof(client_addr);
        int client_sock = accept(proxy_sock, reint_cast<sockaddr *>(&client_addr), &client_len);
        if (client_sock < 0)
            die("Failed to accept client socket\n");

        char req_buf[BUFSIZ] = {0};
        ssize_t reqlen = recv(client_sock, req_buf, BUFSIZ - 1, 0);
        if (reqlen < 0)
            die("Failed to receiving from client\n");

        char hostname[100];
        char port[6] = "80";
        for (char *hostptr = req_buf; *hostptr != '\0'; hostptr++) {
            if (*hostptr == 'H' && memcmp(hostptr, "Host: ", 6) == 0) {
                hostptr += 6;
                for (int i = 0; *hostptr != '\r' && *hostptr != ':' && i < 99; i++, hostptr++) {
                    hostname[i] = *hostptr;
                    hostname[i + 1] = 0;
                }
                if (*hostptr == ':') {
                    hostptr++;
                    for (int i = 0; *hostptr != '\r' && i < 5; i++, hostptr++) {
                        port[i] = *hostptr;
                        port[i + 1] = 0;
                    }
                }
                break;
            }
        }

        struct addrinfo hint, *hostinfo;
        hint.ai_family = AF_INET;
        hint.ai_socktype = SOCK_STREAM;
        if (getaddrinfo(hostname, port, &hint, &hostinfo) != 0)
            die("Can't find host");

        in_addr_t ip = reint_cast<struct in_addr *>(hostinfo->ai_addr->sa_data + 2)->s_addr;
        std::cout << "Host IP: ";
        std::cout << inet_ntoa(*reint_cast<struct in_addr *>(hostinfo->ai_addr->sa_data + 2));
        std::cout << '\n';
        int server_sock = socket(PF_INET, SOCK_STREAM, 0);
        if (server_sock < 0)
            die("Failed to binding server socket\n");

        struct sockaddr_in server_addr = {AF_INET,
                                          htons(static_cast<in_port_t>(atoi(port))),
                                          {ip},
                                          {0}};
        if (connect(server_sock, reint_cast<struct sockaddr *>(&server_addr), sizeof(server_addr))
            < 0)
            die("Failed to connect to server\n");

        if (send(server_sock, req_buf, reqlen < 0 ? 0 : static_cast<size_t>(reqlen), 0) < 0)
            die("Failed to writing to server\n");

        std::thread client(relay, client_sock, server_sock);
        std::thread server(relay, server_sock, client_sock);
        client.detach();
        server.detach();
    }
    close(proxy_sock);
}
